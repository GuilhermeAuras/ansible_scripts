1 - Suba uma instancia ec2 e instale o ansible e o git:
* apt-get update && apt-get install ansible git -y

2 - Copie a chave .pem da sua configuração da Aws para esse host acima criado, para que o host acima tenha acesso ao host do rancher via ssh:
* scp -i chave.pem chave.pem admin@seu_ip_seu_host_ec2_aws

3 - No host que instalou o Ansible, baixe o projeto abaixo:
* git clone https://gitlab.com/GuilhermeAuras/ansible_scripts.git 

4 - No host que instalou o Ansible, modifique o arquivo /etc/ansible/hosts conforme abaixo:
* cp ansible_scripts/aws/hosts /etc/ansible/
* vim /etc/ansible/hosts

4.1 - coloque o ip do rancher server na linha abaixo:
<br>[aws_rancher]
<br>ip_do_seu_rancher_server

4.2 - coloque a chave ssh nessa linha conforme o exemplo:
<br>ansible_ssh_private_key_file=/home/admin/path_da_seu_arquivo_.pem

5 - No host que instalou o Ansible, rode o playbook do Ansible:
* cd ansible_scripts/aws/
* ansible-playbook playbook_rancher.yaml
<br>
Se tudo estiver certo, o host que contem o Ansible vai logar no host que contem o Rancher via ssh sem pedir senha, e vai instalar os pacotes que estão no playbook.
